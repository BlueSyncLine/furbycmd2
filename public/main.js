// The select control
var codeSelect = null;

// The transmit button
var transmitButton = null;

// The custom command code input
var customCodeInput = null;

// Lock the transmit button while transmitting the command.
function lockButton() {
    transmitButton.disabled = true;
}

// Unlocks the ability to transmit.
function unlockButton() {
    transmitButton.disabled = false;
}

function transmit() {
    var command = parseInt(codeSelect.value);

    // Custom code?
    if (command == -1) {
        command = parseInt(customCodeInput.value);

        if (isNaN(command) || command > 1023 || command < 0) {
            alert("The valid command codes are in the range 0 - 1023.");
            return;
        }
    }

    // Lock the transmit button to avoid multiple commands being played at once.
    lockButton();

    // Transmit
    transmitFurbyCommand(command, unlockButton);
}


function onload() {
    furbyInit();
    codeSelect = document.getElementById("codeSelect");
    transmitButton = document.getElementById("transmitButton");
    customCodeInput = document.getElementById("customCodeInput");
}
